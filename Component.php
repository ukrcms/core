<?php

  namespace Uc;

  /**
   * @author Ivan Scherbak <dev@funivan.com>
   */
  class Component {

    /**
     * Component initialization put here
     */
    public function init() {

    }

  }